#pragma once
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
typedef struct {
    const char *p;
    size_t len;
} StringSlice;

StringSlice StringSliceNew(const char *p, const size_t len);
StringSlice StringSliceFromCStr(const char *c_str);
void StringSlicePrint(StringSlice ss, FILE *fp);
bool StringSliceIsSame(StringSlice ss1, StringSlice ss2);

typedef struct {
    size_t length;
    size_t capacity;
    size_t element_size;
    void *data;
} Vector;

Vector VectorNew(const size_t element_size);
void *VectorAt(Vector *v, const size_t index);
void *VectorBack(Vector *v);
void VectorReserve(Vector *v, const size_t new_capacity);
void *VectorEmplace(Vector *v);
void VectorPush(Vector *v, const void *src);
void VectorPop(Vector *v, void *dst);
void VectorForEach(const Vector *v, void (*func)(void *));
