#include "util.h"
#include <assert.h>
#include <stdlib.h>
#include <string.h>

StringSlice StringSliceNew(const char *p, const size_t len) {
    return (StringSlice){
        .p = p,
        .len = len,
    };
}

StringSlice StringSliceFromCStr(const char *c_str) {
    const size_t len = strlen(c_str);
    return StringSliceNew(c_str, len);
}

void StringSlicePrint(StringSlice ss, FILE *fp) {
    for (size_t i = 0; i < ss.len; i++) {
        fprintf(fp, "%c", ss.p[i]);
    }
}

bool StringSliceIsSame(StringSlice ss1, StringSlice ss2) {
    if (ss1.len != ss2.len) {
        return false;
    }
    if (memcmp(ss1.p, ss2.p, ss1.len) != 0) {
        return false;
    }
    return true;
}

Vector VectorNew(const size_t element_size) {
    return (Vector){
        .length = 0,
        .capacity = 0,
        .element_size = element_size,
        .data = NULL,
    };
}

void *VectorAt(Vector *v, const size_t index) {
    assert(index < v->length);
    return ((char *)v->data) + v->element_size * index;
}

void *VectorBack(Vector *v) {
    if (v->length == 0) {
        return NULL;
    } else {
        return VectorAt(v, v->length - 1);
    }
}

void VectorReserve(Vector *v, const size_t new_capacity) {
    assert(v->length <= v->capacity);
    if (v->capacity >= new_capacity) {
        return;
    }
    void *p = malloc(v->element_size * new_capacity);
    assert(p != NULL);
    if (v->data != NULL) {
        memcpy(p, v->data, v->element_size * v->length);
        free(v->data);
    }
    v->data = p;
    v->capacity = new_capacity;
}

void *VectorEmplace(Vector *v) {
    if (v->length == v->capacity) {
        if (v->capacity <= 8) {
            VectorReserve(v, 16);
        } else {
            VectorReserve(v, v->capacity * 2);
        }
    }
    v->length++;
    void *p = VectorAt(v, v->length - 1);
    assert(v->length <= v->capacity);
    return p;
}

void VectorPush(Vector *v, const void *src) {
    void *p = VectorEmplace(v);
    memcpy(p, src, v->element_size);
}

void VectorPop(Vector *v, void *dst) {
    assert(v->length > 0);
    memcpy(dst, VectorAt(v, v->length - 1), v->element_size);
    v->length--;
}

void VectorForEach(const Vector *v, void (*func)(void *)) {
    const size_t es = v->element_size;
    const size_t n = v->length;
    char *p = v->data;
    for (size_t i = 0; i < n; i++) {
        func(p + es * i);
    }
}
