#!/bin/bash
assert() {
  expected="$1"
  input="$2"

  ./9cc "$input" > tmp.s
  cat tmp.lsp
  actual=$(clisp -i init.lsp tmp.lsp | grep -v '^;' | xargs)
  if [ "$actual" != "$expected" ]; then
    echo "$input => $expected expected, but got $actual"
    exit 1
  fi
  cc -o tmp tmp.s
  ./tmp
  actual="$?"

  if [ "$actual" = "$expected" ]; then
    echo "$input => $actual"
  else
    echo "$input => $expected expected, but got $actual"
    exit 1
  fi
}

# 単一整数
assert 0 "0;"
assert 42 "42;"
# 加減算
assert 15 "5+10;"
assert 12 "5+10-3;"
assert 10 "1+2+3+4;"
assert 5 "5-3+4+2-3;"
# 空白
assert 2 "5 - 3;"
assert 8 "5 + 3;"
assert 8 "  5  +  3;  "
# 四則演算
assert 6 '2 * 3;'
assert 2 '4 / 2;'
assert 7 '1+2*3;'
assert 9 '(1+2)*3;'
assert 55 '1+2+3+4+5+6+7+8+9+10;'
assert 33 '2+5+6*(3+2)-4;'
assert 10 '-10+20;'
assert 3 '+3;'
# 比較
assert 1 '1<2;'
assert 0 '2<2;'
assert 0 '3<2;'
assert 1 '1<=2;'
assert 1 '2<=2;'
assert 0 '3<=2;'
assert 0 '1>2;'
assert 0 '2>2;'
assert 1 '3>2;'
assert 0 '1>=2;'
assert 1 '2>=2;'
assert 1 '3>=2;'
assert 0 '42==57;'
assert 1 '42==42;'
assert 1 '42!=57;'
assert 0 '42!=42;'
assert 1 '2+1<2+2;'
assert 1 '2+1<2+2 == 2*3-5;'
assert 1 '(1<2) + (1==2) + (1>2);'
# 代入
assert 2 'a=2;'
assert 3 'a=2;b=3;'
assert 5 'a=2;b=3;a+b;'
assert 1 'a=2;b=3;b-a;'
assert 1 'a=2;b=3;c=a*b-5;d=c;'
assert 1 'a=b=1;b;'
assert 1 'a=b=1;a;'
assert 1 'foo=1;'
assert 2 'foo=2;'
assert 2 'foo1=2;foo2=3;foo1;'
assert 3 'foo1=2;foo2=3;foo2;'
assert 4 '_foo=4;'
assert 3 'a_b=3;'
# return文
assert 1 'return 1;'
assert 2 'return 2; return 1;'

echo OK
