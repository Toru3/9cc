#pragma once
#include "util.h"
#include <stdbool.h>
// 次のトークンが期待している記号のときには、トークンを1つ読み進めて
// 真を返す。それ以外の場合、読み進めずに偽を返す。
bool consume(const char *op);
// 次のトークンがreturnのときには、トークンを1つ読み進めて
// 真を返す。それ以外の場合、読み進めずに偽を返す。
bool consume_return(void);
// 次のトークンが識別子のときには、トークンを1つ読み進めて
// 文字列を返す。それ以外の場合、読み進めずにNULLを返す。
StringSlice consume_ident(void);
// 次のトークンが期待している記号のときには、トークンを1つ読み進める。
// それ以外の場合にはエラーを報告する。
void expect(const char *op);
// 次のトークンが数値の場合、トークンを1つ読み進めてその数値を返す。
// それ以外の場合にはエラーを報告する。
long expect_number(void);
// 終端かどうかを返す
bool at_eof(void);
// 入力文字列pをトークナイズする
void tokenize(const char *p);
