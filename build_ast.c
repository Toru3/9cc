#include "ast.h"
#include "tokenize.h"
#include "util.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

// ローカル変数の型
typedef struct {
    StringSlice ident;
    size_t offset; // RBPからのオフセット
} LVar;

Vector locals;

static void print_nodes_aux(FILE *fp, const Node *node);
static void print_binary_op(FILE *fp, NodeKind kind, const Node *lhs,
                            const Node *rhs) {
    static const char *ops[] = {
        "c_lt", "c_le", "c_gt", "c_ge", "c_eq", "c_ne",
        "+",    "-",    "*",    "/",    "setq",
    };
    fprintf(fp, "(");
    fprintf(fp, "%s ", ops[kind]);
    print_nodes_aux(fp, lhs);
    fprintf(fp, " ");
    print_nodes_aux(fp, rhs);
    fprintf(fp, ")");
}
static void print_nodes_aux(FILE *fp, const Node *node) {
    if (node->kind == ND_NUM) {
        fprintf(fp, "%ld", node->val);
    } else if (node->kind == ND_LVAR) {
        StringSlicePrint(node->ident, fp);
    } else if (node->kind == ND_RETURN) {
        fprintf(fp, "(return-from main ");
        print_nodes_aux(fp, node->lhs);
        fprintf(fp, ")");
    } else if (node->kind == ND_STMT) {
        print_nodes_aux(fp, node->lhs);
        fprintf(fp, "\n    ");
        print_nodes_aux(fp, node->rhs);
    } else {
        // binary op
        print_binary_op(fp, node->kind, node->lhs, node->rhs);
    }
}
void print_nodes(FILE *fp, const Node *node) {
    fprintf(fp, "(print\n");
    fprintf(fp, "  (block main\n    ");
    print_nodes_aux(fp, node);
    fprintf(fp, "\n  )");
    fprintf(fp, "\n)\n");
}

static const Node *new_node(NodeKind kind, const Node *lhs, const Node *rhs) {
    Node *node = calloc(1, sizeof(Node));
    node->kind = kind;
    node->lhs = lhs;
    node->rhs = rhs;
    return node;
}

static const LVar *search_lvar(StringSlice ss) {
    const size_t len = locals.length;
    for (size_t i = 0; i < len; i++) {
        const LVar *lvar = VectorAt(&locals, i);
        if (StringSliceIsSame(ss, lvar->ident)) {
            return lvar;
        }
    }
    return NULL;
}

static const LVar *LVarNew(StringSlice ss) {
    const LVar *last = VectorBack(&locals);
    const size_t new_offset = last ? last->offset + 8 : 0;
    LVar *lvar = VectorEmplace(&locals);
    lvar->ident = ss;
    lvar->offset = new_offset;
    return lvar;
}

static const Node *new_node_ident(StringSlice ss) {
    Node *node = calloc(1, sizeof(Node));
    node->kind = ND_LVAR;
    node->ident = ss;
    const LVar *lvar = search_lvar(ss);
    if (lvar == NULL) {
        lvar = LVarNew(ss);
    }
    node->offset = lvar->offset;
    return node;
}

static const Node *new_node_num(long val) {
    Node *node = calloc(1, sizeof(Node));
    node->kind = ND_NUM;
    node->val = val;
    return node;
}

static const Node *expr(void);
static const Node *primary(void) {
    if (consume("(")) {
        const Node *node = expr();
        expect(")");
        return node;
    } else {
        const StringSlice ss = consume_ident();
        if (ss.p != NULL) {
            return new_node_ident(ss);
        } else {
            return new_node_num(expect_number());
        }
    }
}

static const Node *unary(void) {
    if (consume("+")) {
        return new_node(ND_ADD, new_node_num(0), primary());
    } else if (consume("-")) {
        return new_node(ND_SUB, new_node_num(0), primary());
    } else {
        return primary();
    }
}

static const Node *mul(void) {
    const Node *node = unary();
    for (;;) {
        if (consume("*")) {
            node = new_node(ND_MUL, node, unary());
        } else if (consume("/")) {
            node = new_node(ND_DIV, node, unary());
        } else {
            return node;
        }
    }
}

static const Node *add(void) {
    const Node *node = mul();
    for (;;) {
        if (consume("+")) {
            node = new_node(ND_ADD, node, mul());
        } else if (consume("-")) {
            node = new_node(ND_SUB, node, mul());
        } else {
            return node;
        }
    }
}

static const Node *relational(void) {
    const Node *node = add();
    for (;;) {
        if (consume("<")) {
            node = new_node(ND_LT, node, add());
        } else if (consume("<=")) {
            node = new_node(ND_LEQ, node, add());
        } else if (consume(">")) {
            node = new_node(ND_GT, node, add());
        } else if (consume(">=")) {
            node = new_node(ND_GEQ, node, add());
        } else {
            return node;
        }
    }
}

static const Node *equality(void) {
    const Node *node = relational();
    for (;;) {
        if (consume("==")) {
            node = new_node(ND_EQ, node, relational());
        } else if (consume("!=")) {
            node = new_node(ND_NEQ, node, relational());
        } else {
            return node;
        }
    }
}

static const Node *assign(void) {
    const Node *node = equality();
    for (;;) {
        if (consume("=")) {
            node = new_node(ND_ASSIGN, node, assign());
        } else {
            return node;
        }
    }
}

static const Node *expr(void) { return assign(); }

static const Node *stmt(void) {
    const Node *node;
    if (consume_return()) {
        node = new_node(ND_RETURN, expr(), NULL);
    } else {
        node = expr();
    }
    expect(";");
    return node;
}

const Node *program(void) {
    locals = VectorNew(sizeof(LVar));
    const Node *node = NULL;
    while (!at_eof()) {
        if (node == NULL) {
            node = stmt();
        } else {
            node = new_node(ND_STMT, node, stmt());
        }
    }
    return node;
}
