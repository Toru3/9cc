#include "ast.h"
#include "codegen.h"
#include "tokenize.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "引数の個数が正しくありません");
        return 1;
    }
    tokenize(argv[1]);
    const Node *node = program();
    {
        FILE *fp = fopen("tmp.lsp", "w");
        print_nodes(fp, node);
        fclose(fp);
    }
    fprintf(stderr, "\n");
    gen(stdout, node);
    return 0;
}
