#include "codegen.h"
#include "ast.h"
#include <stdlib.h>

static void gen_lval(FILE *fp, const Node *node) {
    if (node->kind != ND_LVAR) {
        fprintf(stderr, "左辺値が変数ではありません\n");
        exit(1);
    }
    fprintf(fp, "    lea rax, [rbp - %zu]\n", node->offset);
    fprintf(fp, "    push rax\n");
}

static void gen_aux(FILE *fp, const Node *node) {
    if (node->kind == ND_NUM) {
        fprintf(fp, "    push %ld\n", node->val);
        return;
    }
    if (node->kind == ND_LVAR) {
        gen_lval(fp, node);
        fprintf(fp, "    pop rdi\n");
        fprintf(fp, "    mov rax, [rdi]\n");
        fprintf(fp, "    push rax\n");
        return;
    }
    if (node->kind == ND_RETURN) {
        gen_aux(fp, node->lhs);
        fprintf(fp, "    pop rax\n");
        fprintf(fp, "    mov rsp, rbp\n");
        fprintf(fp, "    pop rbp\n");
        fprintf(fp, "    ret\n");
        return;
    }
    if (node->kind == ND_ASSIGN) {
        gen_aux(fp, node->rhs);
        gen_lval(fp, node->lhs);
        fprintf(fp, "    pop rdi\n");
        fprintf(fp, "    pop rax\n");
        fprintf(fp, "    mov [rdi], rax\n");
        fprintf(fp, "    push rax\n");
        return;
    }
    gen_aux(fp, node->lhs);
    gen_aux(fp, node->rhs);
    fprintf(fp, "    pop rdi\n");
    fprintf(fp, "    pop rax\n");
    switch (node->kind) {
    case ND_LT:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    setl al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_LEQ:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    setle al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_GT:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    setg al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_GEQ:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    setge al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_EQ:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    sete al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_NEQ:
        fprintf(fp, "    cmp rax, rdi\n");
        fprintf(fp, "    setne al\n");
        fprintf(fp, "    movzb rax, al\n");
        break;
    case ND_ADD:
        fprintf(fp, "    add rax, rdi\n");
        break;
    case ND_SUB:
        fprintf(fp, "    sub rax, rdi\n");
        break;
    case ND_MUL:
        fprintf(fp, "    imul rax, rdi\n");
        break;
    case ND_DIV:
        fprintf(fp, "    cqo\n");
        fprintf(fp, "    idiv rdi\n");
        break;
    case ND_STMT:
        fprintf(fp, "    mov rax, rdi\n");
        break;
    default:
        fprintf(stderr, "予期しない種類です %d", node->kind);
        exit(1);
    }
    printf("    push rax\n");
}
void gen(FILE *fp, const Node *node) {
    fprintf(fp, ".intel_syntax noprefix\n");
    fprintf(fp, ".global main\n");
    fprintf(fp, "main:\n");
    fprintf(fp, "    push rbp\n");
    fprintf(fp, "    mov rbp, rsp\n");
    fprintf(fp, "    sub rsp, %zu\n", 8 * locals.length);
    gen_aux(fp, node);
    fprintf(fp, "    pop rax\n");
    fprintf(fp, "    mov rsp, rbp\n");
    fprintf(fp, "    pop rbp\n");
    fprintf(fp, "    ret\n");
}
