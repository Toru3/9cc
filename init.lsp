(mapcar
  (lambda (l)
    (eval
      `(defun ,(car l) (lhs rhs)
        (if (,(cdr l) lhs rhs) 1 0)
      )
    )
  )
  '(
    (c_lt . <)
    (c_le . <=)
    (c_gt . >)
    (c_ge . >=)
    (c_eq . =)
    (c_ne . /=)
  )
)
