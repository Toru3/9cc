#pragma once
#include "util.h"
#include <stdio.h>
// 抽象構文木のノードの種類
typedef enum {
    ND_LT,     // <
    ND_LEQ,    // <=
    ND_GT,     // >
    ND_GEQ,    // >=
    ND_EQ,     // ==
    ND_NEQ,    // !=
    ND_ADD,    // +
    ND_SUB,    // -
    ND_MUL,    // *
    ND_DIV,    // /
    ND_ASSIGN, // =
    ND_STMT,   // 式
    ND_LVAR,   // ローカル変数
    ND_NUM,    // 整数
    ND_RETURN, // return文
} NodeKind;

// 抽象構文木のノードの型
typedef struct Node Node;
struct Node {
    NodeKind kind;     // ノードの型
    const Node *lhs;   // 左辺
    const Node *rhs;   // 右辺
    long val;          // kindがND_NUMの場合のみ使う
    StringSlice ident; // kindがND_LVARの場合のみ使う
    size_t offset;     // kindがND_LVARの場合のみ使う
};

extern Vector locals; // ローカル変数

const Node *program(void);
void print_nodes(FILE *fp, const Node *node);
