CFLAGS=-std=c11 -g -static -Wall -Wextra -Wpedantic
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)
9cc: $(OBJS)
	$(CC) -o 9cc $(OBJS) $(LDFLAGS)

$(OBJS): tokenize.h ast.h codegen.h

test: 9cc
	./test.sh

clean:
	rm -f 9cc *.o *~ tmp*

format:
	clang-format-10 -i $(SRCS) $(wildcard *.h)

check:
	clang -c $(SRCS) -fsyntax-only $(CFLAGS)

.PHONY: test clean format check
