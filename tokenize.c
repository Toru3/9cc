#include "tokenize.h"
#include "util.h"
#include <ctype.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// トークンの種類
typedef enum {
    TK_RESERVED, // 記号
    TK_IDENT,    // 識別子
    TK_NUM,      // 整数トークン
    TK_RETURN,   // returnを表すトークン
    TK_EOF,      // 入力の終わりを表すトークン
} TokenKind;

// トークン型
typedef struct Token {
    TokenKind kind; // トークンの型
    long val;       // kindがTK_NUMの場合、その数値
    StringSlice ss; // トークンのスライス
} Token;

// 現在着目しているトークン
static Vector tokens;
static size_t token_index;

// 入力プログラム
static const char *user_input;

// エラー箇所を報告する
static void error_at(StringSlice ss, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    const ptrdiff_t pos = ss.p - user_input;
    fprintf(stderr, "%s\n", user_input);
    fprintf(stderr, "%*s", (int)pos, " "); // pos個の空白を出力
    fprintf(stderr, "%*s", (int)ss.len, "^");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n");
    exit(1);
}

bool consume(const char *op) {
    const StringSlice ss = StringSliceFromCStr(op);
    const Token *tok = VectorAt(&tokens, token_index);
    if (tok->kind != TK_RESERVED || !StringSliceIsSame(ss, tok->ss)) {
        return false;
    }
    token_index++;
    return true;
}

bool consume_return() {
    const Token *tok = VectorAt(&tokens, token_index);
    if (tok->kind != TK_RETURN) {
        return false;
    }
    token_index++;
    return true;
}

StringSlice consume_ident() {
    const Token *tok = VectorAt(&tokens, token_index);
    if (tok->kind != TK_IDENT) {
        return StringSliceNew(NULL, 0);
    }
    token_index++;
    return tok->ss;
}

void expect(const char *op) {
    const StringSlice ss = StringSliceFromCStr(op);
    const Token *tok = VectorAt(&tokens, token_index);
    if (tok->kind != TK_RESERVED || !StringSliceIsSame(ss, tok->ss)) {
        error_at(tok->ss, "'%s'ではありません", op);
    }
    token_index++;
}

long expect_number() {
    const Token *tok = VectorAt(&tokens, token_index);
    if (tok->kind != TK_NUM) {
        error_at(tok->ss, "数ではありません");
    }
    token_index++;
    return tok->val;
}

bool at_eof() {
    const Token *tok = VectorAt(&tokens, token_index);
    return tok->kind == TK_EOF;
}

static FILE *print_tokens_file_pointer;
static void print_tokens_aux(void *p) {
    const Token *tok = p;
    FILE *fp = print_tokens_file_pointer;
    switch (tok->kind) {
    case TK_RESERVED:
    case TK_IDENT:
    case TK_RETURN:
        StringSlicePrint(tok->ss, fp);
        break;
    case TK_NUM:
        fprintf(fp, "%ld", tok->val);
        break;
    case TK_EOF:
        fprintf(fp, "EOF");
        break;
    }
    fprintf(fp, " ");
}

static void print_tokens(FILE *fp) {
    print_tokens_file_pointer = fp;
    VectorForEach(&tokens, print_tokens_aux);
    fprintf(fp, "\n");
    print_tokens_file_pointer = NULL;
}

static Token *new_token(TokenKind kind, StringSlice ss) {
    Token *tok = VectorEmplace(&tokens);
    memset(tok, 0, sizeof(Token));
    tok->kind = kind;
    tok->ss = ss;
    return tok;
}

static bool is_alnum(const char c) { return isalnum(c) || c == '_'; }

static size_t is_ident(const char *start) {
    const char *end = start;
    if (!(isalpha(*start) || *start == '_')) {
        return 0;
    }
    do {
        end++;
    } while (is_alnum(*end));
    return end - start;
}

void tokenize(const char *p) {
    user_input = p;
    tokens = VectorNew(sizeof(Token));
    token_index = 0;

    while (*p) {
        // 空白文字をスキップ
        if (isspace(*p)) {
            p++;
            continue;
        }

        if ((p[0] == '<' && p[1] == '=') || (p[0] == '>' && p[1] == '=') ||
            (p[0] == '=' && p[1] == '=') || (p[0] == '!' && p[1] == '=')) {
            const StringSlice ss = StringSliceNew(p, 2);
            new_token(TK_RESERVED, ss);
            p += ss.len;
            continue;
        }
        if (*p == '+' || *p == '-' || *p == '*' || *p == '/' || *p == '(' ||
            *p == ')' || *p == '<' || *p == '>' || *p == '=' || *p == ';') {
            const StringSlice ss = StringSliceNew(p, 1);
            new_token(TK_RESERVED, ss);
            p += ss.len;
            continue;
        }

        if (isdigit(*p)) {
            char *new_p;
            const long val = strtol(p, &new_p, 10);
            const StringSlice ss = StringSliceNew(p, new_p - p);
            Token *cur = new_token(TK_NUM, ss);
            cur->val = val;
            p += ss.len;
            continue;
        }

        {
            const StringSlice ss = StringSliceNew(p, is_ident(p));
            if (ss.len > 0) {
                if (StringSliceIsSame(ss, StringSliceFromCStr("return"))) {
                    new_token(TK_RETURN, ss);
                } else {
                    new_token(TK_IDENT, ss);
                }
                p += ss.len;
                continue;
            }
        }
        print_tokens(stderr);
        error_at(StringSliceNew(p, 1), "トークナイズできません");
    }

    new_token(TK_EOF, StringSliceNew(p, 0));
    print_tokens(stderr);
}
